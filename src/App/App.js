import '../index.html';
// le chemin relatif pointe déjà sur le dossier node_modules
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';
import appConfig from '../../app.config';
 
const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
class App {
    domTitle;
    domDescription;
    domBegDate;
    domEndDate;
    domLongitude;
    domLattitude;
    domAddEv;
    map;

    localEvent = [];

    start() {
        this.initHtml();
        this.initMap();

// -------------------------------------------------------------------------------

        this.domAddEv.addEventListener('click', () => {
            let jsonEvent = {
                title: this.domTitle.value,
                description: this.domDescription.value,
                begDate: this.domBegDate.value,
                endDate: this.domEndDate.value,
                longitude: this.domLongitude.value,
                latitude: this.domLattitude.value,
            }
            //alert(jsonEvent.begDate);
            this.localEvent.push( jsonEvent );
            
            localStorage.setItem( appConfig.localStorageName, JSON.stringify( this.localEvent ) );
            
            this.createMarker(jsonEvent);
        });
    }

// -------------------------------------------------------------------------------

    initHtml() {
        this.domTitle = document.querySelector( '#domTitle');
        this.domDescription = document.querySelector( '#domDescription');
        this.domBegDate = document.querySelector( '#domBegDate');
        this.domEndDate = document.querySelector( '#domEndDate');
        this.domLongitude = document.querySelector( '#domLongitude');
        this.domLattitude = document.querySelector( '#domLattitude');
        this.domAddEv = document.querySelector( '#domAddEv');
    }

// -------------------------------------------------------------------------------

    coord( evt ) {
        let Lat = evt.lngLat.lat;
        let xLat = evt.lngLat.lat.toFixed(7);

        let Lng = evt.lngLat.lng;
        let xLng = evt.lngLat.lng.toFixed(7);

        this.domLattitude.value = xLat;
        this.domLongitude.value = xLng;
    }

// -------------------------------------------------------------------------------

    initMap() {
        console.log( 'Application started' );
        mapboxgl.accessToken = 'pk.eyJ1Ijoic291bXMiLCJhIjoiY2tuZm9rZ3YzMmhtMTJ2bXF0bDRoMGMwMSJ9.i81id8WDGDYn59QNA2DX-g';
        this.map = new mapboxgl.Map({
            container: 'map',
            center: { lng: 2.9170059, lat: 42.6909705 },
            zoom: 12,
            customAttribution: 'Événements locaux',
            // maxBounds: [[2.7625387, 42.6874962], [2.8178099, 42.6774791]],
            style: 'mapbox://styles/mapbox/outdoors-v11'
        });

        const localEv = JSON.parse(localStorage.getItem(appConfig.localStorageName))
            if ( localEv != null )
            localEv.forEach(element => {
                this.createMarker( element )
            });
        

        // Ajout d'un bouton controle du zoom
        const zoomCtrl = new mapboxgl.NavigationControl();
        this.map.addControl( zoomCtrl );

        // Ajout d'un écouteur de clic sur la map (fonctionne pareil que addEventListener())
        this.map.on( 'click', (this.coord.bind(this)));
    }

    createMarker(jsonEvent) {
        const pinaise = new mapboxgl.Marker({ color: this.colorMarker(jsonEvent.begDate) });
            // title n'est pas dans la doc, car il s'agit de l'attribut HTML (le marker étant une balise <SVG>, on peut l'utiliser)
            pinaise.getElement().title = `${jsonEvent.title} ${jsonEvent.begDate}`;
    
            // Création de la popup pour le marker
            const popup = new mapboxgl.Popup();
            // Ajout de texte
            popup.setText(jsonEvent.description);
    
            // Attribution de la popup au marker
            pinaise.setPopup( popup );
            // Attribution des coordonnées géo
            
            pinaise.setLngLat({ lng: jsonEvent.longitude, lat: jsonEvent.latitude });
            // Ajout sur la carte
            pinaise.addTo( this.map );
    };
    
    colorMarker(begDate) {
        let Today = new Date().getTime();
        let eventDate = new Date(begDate).getTime();
        let Delay = eventDate - Today;
        const threeDays = 1000 * 60 * 60 * 24 * 3;
        let color = 'red'; // Si l'événement est dépassé
        if ( Delay > threeDays ) { color = 'green';}
        if ( Delay <= threeDays && Delay > 0 ) { color = 'yellow';}
           
        return color;
    };
}

const instance = new App();

export default instance;